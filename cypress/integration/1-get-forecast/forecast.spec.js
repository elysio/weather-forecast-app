/// <reference types="cypress" />

// Welcome to Cypress!
//
// This spec file contains a variety of sample tests
// for a todo list app that are designed to demonstrate
// the power of writing tests in Cypress.
//
// To learn more about how Cypress works and
// what makes it such an awesome testing tool,
// please read our getting started guide:
// https://on.cypress.io/introduction-to-cypress

describe('forecast test', () => {
  it('Toronto Forecast Test', function() {

    cy.viewport(783, 787)
 
    cy.visit('http://localhost:5000/')

    cy.contains('Weather forecast').should('be.visible');
 
    cy.get('[data-cy=citySelect]').click()

    cy.get('body > .p-dropdown-panel > .p-dropdown-items-wrapper > .p-dropdown-items > .p-dropdown-item:nth-child(1)').click()
 
    cy.get('[data-cy=btnGetForecast]').click()
 
    cy.contains('Forecast').should('be.visible');

    cy.get('.tableBody').find('tr').its('length').should('be.gte', 1)

    cy.get('#app > div > div > .navbar > .p-button:nth-child(2)').click()
 
    cy.get('.tableBody').find('tr').its('length').should('be.gte', 1)

    cy.get('div > div > .navbar > .p-button:nth-child(3) > .p-button-label').click()
 
    cy.get('.tableBody').find('tr').its('length').should('be.gte', 1)

    cy.get('div > div > .navbar > .p-button:nth-child(4) > .p-button-label').click()
 
    cy.get('.tableBody').find('tr').its('length').should('be.gte', 1)

    cy.get('div > div > .navbar > .p-button:nth-child(5) > .p-button-label').click()
 
    cy.get('.tableBody').find('tr').its('length').should('be.gte', 1)

    cy.get('div > div > .navbar > .p-button:nth-child(6) > .p-button-label').click()
 
    cy.get('.tableBody').find('tr').its('length').should('be.gte', 1)

    })

    it('Ottawa Forecast Test', function() {

      cy.viewport(783, 787)
   
      cy.visit('http://localhost:3001/')
  
      cy.contains('Weather forecast').should('be.visible');
   
      cy.get('[data-cy=citySelect]').click()
  
      cy.get('body > .p-dropdown-panel > .p-dropdown-items-wrapper > .p-dropdown-items > .p-dropdown-item:nth-child(2)').click()
   
      cy.get('[data-cy=btnGetForecast]').click()
   
      cy.contains('Forecast').should('be.visible');
  
      cy.get('.tableBody').find('tr').its('length').should('be.gte', 1)
  
      cy.get('#app > div > div > .navbar > .p-button:nth-child(2)').click()
   
      cy.get('.tableBody').find('tr').its('length').should('be.gte', 1)
  
      cy.get('div > div > .navbar > .p-button:nth-child(3) > .p-button-label').click()
   
      cy.get('.tableBody').find('tr').its('length').should('be.gte', 1)
  
      cy.get('div > div > .navbar > .p-button:nth-child(4) > .p-button-label').click()
   
      cy.get('.tableBody').find('tr').its('length').should('be.gte', 1)
  
      cy.get('div > div > .navbar > .p-button:nth-child(5) > .p-button-label').click()
   
      cy.get('.tableBody').find('tr').its('length').should('be.gte', 1)
  
      cy.get('div > div > .navbar > .p-button:nth-child(6) > .p-button-label').click()
   
      cy.get('.tableBody').find('tr').its('length').should('be.gte', 1)
  
      })

      it('Tokio Forecast Test', function() {

        cy.viewport(783, 787)
     
        cy.visit('http://localhost:3001/')
    
        cy.contains('Weather forecast').should('be.visible');
     
        cy.get('[data-cy=citySelect]').click()
    
        cy.get('body > .p-dropdown-panel > .p-dropdown-items-wrapper > .p-dropdown-items > .p-dropdown-item:nth-child(3)').click()
     
        cy.get('[data-cy=btnGetForecast]').click()
     
        cy.contains('Forecast').should('be.visible');
    
        cy.get('.tableBody').find('tr').its('length').should('be.gte', 1)
    
        cy.get('#app > div > div > .navbar > .p-button:nth-child(2)').click()
     
        cy.get('.tableBody').find('tr').its('length').should('be.gte', 1)
    
        cy.get('div > div > .navbar > .p-button:nth-child(3) > .p-button-label').click()
     
        cy.get('.tableBody').find('tr').its('length').should('be.gte', 1)
    
        cy.get('div > div > .navbar > .p-button:nth-child(4) > .p-button-label').click()
     
        cy.get('.tableBody').find('tr').its('length').should('be.gte', 1)
    
        cy.get('div > div > .navbar > .p-button:nth-child(5) > .p-button-label').click()
     
        cy.get('.tableBody').find('tr').its('length').should('be.gte', 1)
    
        cy.get('div > div > .navbar > .p-button:nth-child(6) > .p-button-label').click()
     
        cy.get('.tableBody').find('tr').its('length').should('be.gte', 1)
    
        })

  
})


