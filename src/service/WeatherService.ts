import axios from 'axios';
import { format } from 'date-fns';
import _ from 'lodash';
export default class WeatherService {

    async getCitiesList(){
        return [
                        {
                        "id": 6167865,
                        "name": "Toronto",
                        "country": "CA",
                        get label() {
                            return this.name + ', ' + this.country;
                        }
                        },
                        {
                        "id": 6094817,
                        "name": "Ottawa",
                        "country": "CA",
                        get label() {
                            return this.name + ', ' + this.country;
                        }
                        },
                        {
                        "id": 1850147,
                        "name": "Tokyo",
                        "country": "JP",
                        get label() {
                            return this.name + ', ' + this.country;
                        }
                        }
                ]
    }
    
    async getCurrentWeather(cityId: number) {
        return axios.get('http://api.openweathermap.org/data/2.5/weather?id='+cityId+'&appid=538882fc8387290c6cee83f313a6acf5&units=metric')
            .then(res => res.data);
    }

    async getForecast(cityId: number) {
        
        return axios.get('http://api.openweathermap.org/data/2.5/forecast?id='+cityId+'&appid=538882fc8387290c6cee83f313a6acf5&units=metric')
            .then(res => {
                if(res.data&&res.data.list){
                    const dateDesc = (item: { dt: any; }) => this.formatDate(item.dt);
                    const result = _.groupBy(res.data.list, dateDesc);
                    return result;
                }
            });
    }
    formatDate(date:number){
        return format(date*1000, "d MMM").toUpperCase(); 
    }
}